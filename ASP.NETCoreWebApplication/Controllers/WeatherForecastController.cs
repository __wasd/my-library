﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ASP.NETCoreWebApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IHandler<AutoDto,string> _gearmanyAutuZavod;
        
        public WeatherForecastController(ILogger<WeatherForecastController> logger,IHandler<AutoDto,string> gearmanyAutuZavod)
        {
            _logger = logger;
            _gearmanyAutuZavod = gearmanyAutuZavod;
        }

        [HttpGet]
        public AutoDto Get()
        {

            var rng = new Random();

            return _gearmanyAutuZavod.Handler(rng.ToString());
            
        }
        
        
    }
}